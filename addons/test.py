# -*- coding: utf-8 -*-
# import the main window object (mw) from aqt
from aqt import mw
# import the "show info" tool from utils.py
from aqt.utils import showInfo
from aqt import QAction


# We're going to add a menu item below. First we want to create a function to
# be called when the menu item is activated.dddddddddd


def test_function():
    """dfkjöadkölajgöaöajö
    """
    # get the number of cards in the current collection, which is stored dtein
    # the main window
    deck_obj = mw.col.decks
    print_val = "\n".join(deck_obj.allNames())
    # show a message box
    showInfo(print_val)


# create a new menu item, "test"
ACTION = QAction("test", mw)
# set it to call testFunction when it's clicked
ACTION.triggered.connect(test_function)
# and add it to the tools menu
mw.form.menuTools.addAction(ACTION)
